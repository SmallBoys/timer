package com.zhanghe;

public class Mission {

	private Runnable mission;
	
	private long repeattime = 0;

	public Mission(Runnable mission , long repeattime ){
		super();
		this.mission = mission;
		this.repeattime = repeattime;
	}

	public Runnable getMission() {
		return mission;
	}

	public void setMission( Runnable mission ) {
		this.mission = mission;
	}

	public long getRepeattime() {
		return repeattime;
	}

	public void setRepeattime( long repeattime ) {
		this.repeattime = repeattime;
	}
	
}
