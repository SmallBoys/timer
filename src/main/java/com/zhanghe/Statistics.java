package com.zhanghe;

import java.util.concurrent.atomic.AtomicLong;

public class Statistics {
	
	public AtomicLong total_add_num;
	
	public AtomicLong total_excute_num;
	
	public AtomicLong expire_num;
	
	public AtomicLong total_timeout;

	public AtomicLong loop_num;

	public AtomicLong loop_time;
	
	public Statistics(){
		this.total_add_num = new AtomicLong(0);
		this.total_excute_num = new AtomicLong(0);
		this.expire_num = new AtomicLong(0);
		this.total_timeout = new AtomicLong(0);
		this.loop_num = new AtomicLong(0);
		this.loop_time = new AtomicLong(0);
	}
	public void loop(long cost){
		loop_num.getAndAdd(1);
		loop_time.getAndAdd(cost);
	}
	public void exucteMission(long timeout){
		if(timeout>0&&timeout<1300){
			total_timeout.addAndGet(timeout);
			total_excute_num.addAndGet(1);
		}else{
			total_timeout.addAndGet(timeout);
			expire_num.addAndGet(1);
		}
	}
	
	public void addMission(){
		total_add_num.addAndGet(1);
	}
	
	public float avg_time(){
		return total_timeout.longValue()/total_excute_num.longValue();
	}
	
	public float timeout_rate(){
		return expire_num.longValue()/total_excute_num.longValue();
	}

	public float loop_rate(){
		return this.loop_time.longValue()/this.loop_num.longValue();
	}
}
