package com.zhanghe;

import java.util.Queue;

public class PriorityQueueBarrel {

	private Queue<Mission> missions;

	private long repeat_time;

	public PriorityQueueBarrel(Queue<Mission> missions ){
		super();
		this.missions = missions;
		this.repeat_time = 0;
	}

	public Queue<Mission> getMissions() {
		return missions;
	}

	public void setMissions( Queue<Mission> missions ) {
		this.missions = missions;
	}

	public long getRepeat_time() {
		return repeat_time;
	}

	public void setRepeat_time(long repeat_time) {
		this.repeat_time = repeat_time;
	}
}
