package com.zhanghe;

public class TimerBuilder {

	private int barrel_size;
	
	private int threadNum = 5;
	
	public TimerBuilder(){
		super();
	}

	public TimerBuilder setBarrel_size( int barrel_size ) {
		this.barrel_size = barrel_size;
		return this;
	}

	public TimerBuilder setThreadNum( int threadNum ) {
		this.threadNum = threadNum;
		return this;
	}

	public AbstractTimer Create(){
		return new AbstractTimer(barrel_size,threadNum);
	}

	public PriorityQueueTimer CreatePriorityQueueTimer(){
		return new PriorityQueueTimer(barrel_size,threadNum);
	}
}
