package com.zhanghe;

import java.util.Queue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class Barrel {

    private Queue<Mission> missions;
    
    private ReadWriteLock lock = null;
    private Lock r = null;
    private Lock w = null;
    
    public Barrel( Queue<Mission> missions ){
        super();
        this.missions = missions;
        this.lock = new ReentrantReadWriteLock();
        this.r = lock.readLock();
        this.w = lock.writeLock();
    }

    public Queue<Mission> getMissions() {
        return missions;
    }

    public void setMissions( Queue<Mission> missions ) {
        this.missions = missions;
    }

	public ReadWriteLock getLock() {
		return lock;
	}

	public void setLock( ReadWriteLock lock ) {
		this.lock = lock;
	}

	public Lock getR() {
		return r;
	}

	public void setR( Lock r ) {
		this.r = r;
	}

	public Lock getW() {
		return w;
	}

	public void setW( Lock w ) {
		this.w = w;
	}

}
