package com.zhanghe;

public abstract class RunnableMission implements Runnable{

	public Statistics statistics;
	
	public abstract void setStatistics( Statistics statistics );
	
}
