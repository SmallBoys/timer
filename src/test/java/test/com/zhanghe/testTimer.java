package test.com.zhanghe;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zhanghe.AbstractTimer;
import com.zhanghe.TimerBuilder;


public class testTimer {
	final static Logger logger = LoggerFactory.getLogger(testTimer.class);
	/**
	 * 
	 * 功能：测试大量任务<br/>
	 * @throws InterruptedException
	 * @exception   无
	 * @since   	首次创建（zhanghe  2018-5-4)<br/>
	 *
	 */
    @Test
	public void testManyMission() throws InterruptedException{
    	logger.debug("start");
    	AbstractTimer timer = new TimerBuilder().setBarrel_size(10).setThreadNum(10).Create();
    	Random r = new Random();
    	ExecutorService execforTimer = Executors.newSingleThreadExecutor();
        ExecutorService execforCnacle = Executors.newSingleThreadExecutor();
    	final Future<?> f = execforTimer.submit(timer);
        ExecutorService exec = Executors.newFixedThreadPool(5);

        exec.submit(new ManyMissionCase(timer,r));
        exec.submit(new ManyMissionCase(timer,r));
        exec.submit(new ManyMissionCase(timer,r));
        exec.submit(new ManyMissionCase(timer,r));
        exec.submit(new ManyMissionCase(timer,r));
        exec.submit(new ManyMissionCase(timer,r));
        exec.submit(new ManyMissionCase(timer,r));
        exec.submit(new ManyMissionCase(timer,r));
        exec.submit(new ManyMissionCase(timer,r));
        exec.submit(new ManyMissionCase(timer,r));
        exec.submit(new ManyMissionCase(timer,r));
        exec.submit(new ManyMissionCase(timer,r));
        exec.submit(new ManyMissionCase(timer,r));
        exec.submit(new ManyMissionCase(timer,r));
        exec.submit(new ManyMissionCase(timer,r));


        execforCnacle.submit(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(501000);
                    f.cancel(true);
                    System.out.println("diaoyong interrupt");
                }
                catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
        execforTimer.shutdown();
        while (!execforTimer.awaitTermination(100, TimeUnit.SECONDS)) {
        }
        execforCnacle.shutdown();
        while (!execforCnacle.awaitTermination(100, TimeUnit.SECONDS)) {
        }
        System.out.println("添加任务总数:"+timer.statistics.total_add_num.longValue()+",执行任务总数:"+timer.statistics.total_excute_num.longValue()+",平均延迟:"
                +timer.statistics.avg_time()+",超时率:"+timer.statistics.timeout_rate()+",遍历平均时间:"+timer.statistics.loop_rate());
        exec.shutdown();
        while (!exec.awaitTermination(100, TimeUnit.SECONDS)) {
        }
	}
    static class ManyMissionCase implements Runnable{
    	private AbstractTimer timer;
    	private Random r;
    	public ManyMissionCase(AbstractTimer timer, Random r){
    		this.timer = timer;
    		this.r = r;
    	}
        public void run() {
            int j=0;
            while(j<100000){
                int i = 0;
//                try{
//                    Thread.sleep(r.nextInt(100));
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
                int timeout = r.nextInt(1000);
                timer.addmession(timeout, new TestMission(timeout,new Date()));
                j++;
            }
        }
    }
}
