package test.com.zhanghe;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zhanghe.AbstractTimer;
import com.zhanghe.TimerBuilder;

public class test {
	final static Logger logger = LoggerFactory.getLogger(testTimer.class);
	
	public static void main( String[] args ) {
		logger.debug("start");
    	AbstractTimer timer = new TimerBuilder().setBarrel_size(10).setThreadNum(10).Create();
    	Random r = new Random();
    	ExecutorService execforTimer = Executors.newSingleThreadExecutor();
        
    	final Future<?> f = execforTimer.submit(timer);
        ExecutorService exec = Executors.newFixedThreadPool(6);
        
        exec.submit(new ManyMissionCase(timer,r));
        exec.submit(new ManyMissionCase(timer,r));
        exec.submit(new ManyMissionCase(timer,r));
        exec.submit(new ManyMissionCase(timer,r));
        exec.submit(new ManyMissionCase(timer,r));
        
        
        exec.submit(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(20000);
                    f.cancel(true);
                    System.out.println("diaoyong interrupt");
                }
                catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });
        exec.shutdown();
	}
    static class ManyMissionCase implements Runnable{
    	private AbstractTimer timer;
    	private Random r;
    	public ManyMissionCase(AbstractTimer timer, Random r){
    		this.timer = timer;
    		this.r = r;
    	}
        public void run() {
            int j=0;
            while(j<100000){
                int i = 0;
                int timeout = r.nextInt(1000);
                timer.addmession(9, new TestMission(9,new Date()));
                j++;
            }
            System.out.println("插入完毕");
        }
    }
}
