package test.com.zhanghe;
import com.zhanghe.Mission;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * testPriorityQueue
 *
 * @author Clevo
 * @date 2018/4/27
 */
public class testPriorityQueue {
    public static void main(String[] args) {
        Comparator<Mission> comparator = new Comparator<Mission>() {
            public int compare(Mission o1, Mission o2) {
                if(o1.getRepeattime()==o2.getRepeattime()){
                    return 0;
                }else{
                    return o1.getRepeattime()>o2.getRepeattime() ? 1 : -1;
                }
            }
        };
        PriorityQueue<Mission> queue =new PriorityQueue<Mission>(10,comparator);

        queue.offer(new Mission(null,1));
        queue.offer(new Mission(null,5));
        queue.offer(new Mission(null,2));
        queue.offer(new Mission(null,28));
        queue.offer(new Mission(null,11));
        queue.offer(new Mission(null,16));

        System.out.println(queue.poll().getRepeattime());
        System.out.println(queue.poll().getRepeattime());
    }
}
