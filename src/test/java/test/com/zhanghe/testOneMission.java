package test.com.zhanghe;
import com.zhanghe.PriorityQueueTimer;
import com.zhanghe.TimerBuilder;

import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 * testOneMission
 *
 * @author Clevo
 * @date 2018/5/2
 */
public class testOneMission {

    static PriorityQueueTimer timer = new TimerBuilder().setBarrel_size(10).setThreadNum(10).CreatePriorityQueueTimer();

    public static void main(String[] args) throws InterruptedException {
        ExecutorService execforTimer = Executors.newSingleThreadExecutor();
        final Future<?> f = execforTimer.submit(timer);
        timer.addmession(0,new TestMission(0,new Date()));
        timer.addmession(1,new TestMission(1,new Date()));
        timer.addmession(3,new TestMission(3,new Date()));
        timer.addmession(3,new TestMission(3,new Date()));
        timer.addmession(3,new TestMission(3,new Date()));
        timer.addmession(3,new TestMission(3,new Date()));
        timer.addmession(3,new TestMission(3,new Date()));
        timer.addmession(3,new TestMission(3,new Date()));
        timer.addmession(11,new TestMission(11,new Date()));
        timer.addmession(10,new TestMission(10,new Date()));

        f.cancel(true);
        execforTimer.shutdown();
        while (!execforTimer.awaitTermination(100, TimeUnit.SECONDS)) {
        }
    }
}
